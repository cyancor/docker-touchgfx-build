FROM ubuntu:focal
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV DEBIAN_FRONTEND="noninteractive" \
    PATH="$PATH:/gcc-arm-none-eabi/bin"

# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

RUN apt-get update \
    && apt-get install --yes \
        git \
        cmake \
        clang \
        clang-tidy \
        clang-format \
        ccache \
        ruby-full \
        ruby-dev \
        zlib1g-dev \
        apt-utils \
        curl \
        software-properties-common \
        zip \
        unzip \
        gnupg2 \
        libtclap-dev \
        python \
# PIP
    && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python get-pip.py \
    && rm get-pip.py \
# cppclean
    && pip install --upgrade cppclean \
# ccache
    && mkdir -p /.ccache \
    && chmod 0777 /.ccache \
# Compilers
    && curl -o /gcc-arm-none-eabi.tar.bz2 "https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2" \
    && tar xjf /gcc-arm-none-eabi.tar.bz2 \
    && rm /gcc-arm-none-eabi.tar.bz2 \
    && mv /gcc-arm-none-eabi-* /gcc-arm-none-eabi \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt-get update \
    && apt-get install --yes \
        g++-9 \
        mingw-w64 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 \
    && update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix \
    && update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix \
    && update-alternatives --set i686-w64-mingw32-gcc /usr/bin/i686-w64-mingw32-gcc-posix \
    && update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix \
# Additional tools
    && gem install roo \
# Cleanup
    && rm -rf /var/lib/apt/lists/* \
    && arm-none-eabi-gcc --version
